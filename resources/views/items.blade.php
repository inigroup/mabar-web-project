@extends('template.master')

@section('konten')
    
<h1>ITEM LIST</h1>

    <div class="card mb-4" >
        <div class="card-header">Dashboard</div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            You are logged in!
        </div>
    </div>


    <table width="100%">
    <thead>
        <!-- tinggal add css-->
        <tr> <!--Table column name-->
            <th>No</th>
            <th>Nama</th>
            <th>Tanggal</th>
            <th>Jam</th>
            <th>Pemain</th>
            <th>Opsi</th>
        </tr>
                
    </thead>
        @foreach($items as $p)
        <tr>
            <td>{{ $p->id }}</td>
            <td>{{ $p->name }}</td>
            <td>{{ $p->date }}</td>
            <td>{{ $p->time }}</td>
            <td>{{ $p->person }}</td>
            <td>
                <a href="/items/edit/{{ $p->id }}">Edit</a>
                |
                <a href="/items/hapus/{{ $p->id }}">Hapus</a>
            </td>
            {{-- $table->bigIncrements('id');
            $table->string('name');
            $table->date('date');
            $table->time('time');
            $table->integer('person'); --}}
        </tr>
        @endforeach
            
    </table>
    <!-- pagination halaman user -->
            Halaman             : {{ $items->currentPage() }}   <br/>
            Jumlah Data         : {{ $items->total() }}         <br/>
            Data Per Halaman    : {{ $items->perPage() }}
            {{ $items->links()}}
 
</body>
</html>

@endsection