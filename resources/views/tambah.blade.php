@extends('template.master')

@section('title')
    Tambah Item
@endsection

@section('addItem')
<div class="container">
    <form action="/items/store" method="post">
    
        {{ csrf_field() }}
        <div class="form-group">
        <label for="name">nama</label>
        <input type="text"      class="form-control"      name="name"         value=""    placeholder="nama"><br>
        </div>

        <div class="form-group">
        <label for="desc">tanggal</label>
        <input type="date"      class="form-control"      name="date"    value=""    placeholder="tanggal main"><br>
        </div>

        <div class="form-group">
        <label for="pric">jam</label>
        <input type="time"    class="form-control"    name="time"        value=""    placeholder="mulai pukul"><br>
        </div>

        <div class="form-group">
        <label for="Stok">pemain</label>
        <input type="number"    class="form-control"    name="person"         value=""    placeholder="jumlah pemain"><br>
        </div>
        <!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->

        <input type="submit" name="simpan" value="simpan">
    </form>
</div>
@endsection