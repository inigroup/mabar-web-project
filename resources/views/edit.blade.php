@extends('template.master')

@section('title')
    Edit
@endsection

@section('konten')
<div class="container">
    @foreach($items as $p)

    <form action="/items/update" method="post">
    
        {{ csrf_field() }}
        <div class="form-group">
        <input type="hidden"    name="id"           value="{{ $p->id }}"><br/>
        </div>
        
                <div class="form-group">
                <label for="name">nama</label>
                <input type="text"      class="form-control"      name="name"         value="{{$p->name}}"    placeholder="nama"><br>
                </div>
        
                <div class="form-group">
                <label for="desc">tanggal</label>
                <input type="date"      class="form-control"      name="date"    value="{{$p->date}}"    placeholder="tanggal main"><br>
                </div>
        
                <div class="form-group">
                <label for="pric">jam</label>
                <input type="time"    class="form-control"        name="time"        value="{{$p->time}}"    placeholder="mulai pukul"><br>
                </div>
        
                <div class="form-group">
                <label for="Stok">pemain</label>
                <input type="number"    class="form-control"      name="person"         value="{{$p->person}}"    placeholder="jumlah pemain"><br>
                </div>
                <!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
        
                <input type="submit" name="simpan" value="simpan">
    </form>
    
	@endforeach
</div>

@endsection