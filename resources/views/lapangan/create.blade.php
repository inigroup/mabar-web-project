@extends('layouts.app')

@section('content')

<form method="POST" action="{{ route('lapangan.create') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
<div class="container">
    <form class="" action="index" method="post">
        <div class="form-group">
            <label for="">Title</label>
            <input type="text" class="form-control" name="title" placeholder="Lapangan title">
        </div>

        <div class="form-group">
            <label for="">Content</label>
            <textarea name="content" row="5" class="form-control"></textarea>
        </div>


    </form>
</div>
</form>
@endsection