@extends('template.master')

@section('konten')

@foreach ($items as $item)
<div class="card" style="width: 18rem;">
  <div class="card-body">
    <h5 class="card-title">{{$item->name}} </h5>
    <h6 class="card-subtitle mb-2 text-muted">Tanggal Booking : {{$item->date}}</h6>
    <p class="card-text">Jumlah pemain : {{$item->person}} </p>
    <a href="/items/tambah" class="card-link">Join</a>
    <a href="#" class="card-link">Another link</a>
  </div>
</div>
@endforeach

@endsection