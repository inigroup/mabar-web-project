<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BookingController extends Controller
{
    public function booking(Request $request)
    {
        // insert data ke table ecommerce
        DB::table('items')->insert([
            'name'          => $request->name,
            'date'          => $request->date,
            'time'          => $request->time,
            'person'        => $request->person
        ]);
        // alihkan halaman ke halaman items
        return redirect('/items');
    }
}
