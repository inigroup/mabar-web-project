<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class ItemController extends Controller
{
    public function index()
    {
        // mengambil data pegawai
        
    	$items = Item::paginate(10);
 
    	// mengirim data pegawai ke view pegawai
    	return view('items', ['items' => $items]);
    }

    public function tambah(){

        return view('tambah');
    }

    public function store(Request $request)
    {
        // insert data ke table ecommerce
        DB::table('items')->insert([
            'name'          => $request->name,
            'date'   => $request->date,
            'time'         => $request->time,
            'person'         => $request->person
        ]);
        // alihkan halaman ke halaman items
        return redirect('/items');
    }


    //edit data DB
    public function edit($id){
        
        $items = DB::table('items')->where('id',$id)->get();
        
        return view('edit', ['items'    => $items]);
 
    }

    public function update(Request $request)
    {
        // update data DB
        //$ecommerce = Post:all();
        DB::table('items')->where('id',$request->id)->update([
            'name'          => $request->name,
            'date'          => $request->date,
            'time'          => $request->time,
            'person'        => $request->person
        ]);
        
        // tampilkan kembali database yang sudah di edit
        return redirect('items');
    }

    //delete isi DB berdasarkan ID
    public function hapus($id){
        DB::table('items')->where('id',$id)->delete();

    // tampilkan kembali database yang sudah di edit
    return redirect('items');

    }
    
    public function muka(){
        $items = DB::table('items')->get();
        return view('muka' , ['items' => $items]);
    }
    
}
