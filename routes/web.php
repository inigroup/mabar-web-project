<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ItemController@muka');

Route::get('dashboard', function () {
	return view('dashboard');
});
Route::get  ('/items',              'ItemController@index');
//route crud di page items
Route::get  ('/items/items',         'ItemController@items');
Route::get  ('/items/tambah',        'ItemController@tambah')->middleware('auth');
Route::post ('/items/store',         'ItemController@store');

//edit and delete DATABASE
Route::get  ('/items/edit/{id}',     'ItemController@edit');
Route::post ('/items/update',        'ItemController@update');
Route::get  ('/items/hapus/{id}',    'ItemController@hapus');

//route booking user

//dashboard
Route::get  ('/muka',              'MukaController@muka');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//
Route::get('/lapangan/create', "LapanganController@create")->name('lapangan.create');
