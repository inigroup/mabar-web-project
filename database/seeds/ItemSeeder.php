<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ItemSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create('id_ID');
    
            for($i = 1; $i <= 20; $i++){
    
                // insert data ke table pegawai menggunakan Faker
                DB::table('items')->insert([
                    'name' => $faker->name,
                    'date' => $faker->date,
                    'time' => $faker->time,
                    'person' => $faker->numberBetween(0,15),
                ]);
    
        }
    }

}
